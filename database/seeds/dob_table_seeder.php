<?php

use Illuminate\Database\Seeder;

class dob_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dob')->insert([
            ['name' => 'Hugh Jackman', 'birthdate' => \Carbon\Carbon::create(1968,10,12,10,15,00) , 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()],
        ]);

        factory('wstest\dob', 4)->create();
    }
}
