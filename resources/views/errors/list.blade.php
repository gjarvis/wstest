@if($errors->any())
<div class="card-panel white">
    <ul>
        @foreach ($errors->all() as $error)
        <li>
            {{ $error }}

        </li>
        @endforeach
    </ul>
</div>
@endif