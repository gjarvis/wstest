<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <title>WS Test</title>
</head>
<body>
<div class="container">
    <div class="errors row">
        @include('errors.list')
    </div>
    @yield('content')
</div>

<div class="container">
    @yield('footer')
</div>

</body>
</html>