@extends('app')

@section('content')

<div class="row">
    <div class="col-md-6 col-md-offset-2">
        <h1>Calculate your age</h1>
    </div>
</div>



<hr />

{!! Form::open(['url' => 'dob']) !!}

<div class="row">
    <div class="form-group col-md-6 col-md-offset-1">

        {!! Form::label('name', 'Name: ') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}

    </div>
</div>

<div class="row">
    <div class="form-group col-md-2">

        {!! Form::label('year', 'Year of birth: ') !!}
        {!! Form::text('year', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-2">

        {!! Form::label('month', 'Month:') !!}
        {!! Form::select('month', $months, null, array('class' => 'form-control validate','required' => 'required')) !!}


    </div>

    <div class="form-group col-md-2">

        {!! Form::label('day', 'Day of month:') !!}
        {!! Form::select('day', $days, null, array('class' => 'form-control validate','required' => 'required')) !!}

    </div>

    <div class="form-group col-md-2">

        {!! Form::label('hour', 'Hour of day:') !!}
        {!! Form::select('hour', $hours, null, array('class' => 'form-control validate','required' => 'required')) !!}

    </div>

</div>

<div class="row">
    <div class="form-group col-md-4 col-md-offset-2">
        {!! Form::submit('Submit your details', ['class' => 'btn-primary form-control']) !!}
    </div>
</div>



{!! Form::close() !!}

@stop