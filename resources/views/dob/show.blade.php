@extends('app')

@section('content')

<h1>{{$dob->name}}</h1>
<h3>Your Age is:</h3>
<h4>{{$age}}</h4>

<div class="form-group">
    <a type="button" class="btn btn-primary" href="/dob" role="button">List all submissions</a>
</div>

@stop