@extends('app')

@section('content')
<h1>Dates of birth</h1>

<table class="table table-striped">
    <tr>
        <th>Name</th>
        <th>Date of Birth</th>
        <th>Age</th>
    </tr>

    @foreach ($dobs as $dob)
    <tr>
        <td>{{$dob->name}}</td>
        <td>{{$dob->birthdate}}</td>
        <td>{{$dob->age}}</td>
    </tr>
    @endforeach

</table>

<div class="form-group">
    <a type="button" class="btn btn-primary" href="/dob/create" role="button">Calculate your own age</a>
</div>

@stop