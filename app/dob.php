<?php

namespace wstest;

use Illuminate\Database\Eloquent\Model;

class dob extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dob';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birthdate'
    ];
}
