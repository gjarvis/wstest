<?php

namespace wstest\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;use wstest\dob;
use wstest\Http\Requests;

class DobController extends Controller
{

    /**
     * Setup some basic arrays for first code iteration
     */

    protected $months = [1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec'];
    protected $days = [1 => 'Mon', '2' => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat', 7 => 'Sun'];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $day = $data['day'] + 1;
        $data['date_of_birth'] = $day.'-'.$data['month'].'-'.$data['year'];
        $current_year          = Carbon::now()->year;
        $hundred_years_ago     = (new Carbon("100 years ago"))->year;

        // create an array of the validation rules
        $rules = [
            'name' => 'required|alpha|min:3|max:50',
            'year'          => 'Required|Integer|Between:'.$hundred_years_ago.','.$current_year,
            'date_of_birth' => 'Required|Date|Before:'.Carbon::today(),
        ];

        // create an array of custom error messages
        $messages = [
            'name.max'     => 'Name cannot be more than 50 characters',
            'date_of_birth.before' => 'Date must be before today',
        ];

        // create the validator including the data, rules and custom messages
        $dataValidator = Validator::make($data, $rules, $messages);

        // Validate arrays of data values
        $dataValidator->each('attachments',['max:10240']); // max size of attachment in KB
        $dataValidator->each('category_id', ['exists:categories,id']);

        // return the validator
        return $dataValidator;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dobs = dob::all();

        foreach ($dobs as $key => $loop)
        {
            $loop->age = Carbon::now()->diff(new Carbon($loop->birthdate))->format('%y years, %m months, %d days, %h hours');
        }

        return view('dob.index', compact('dobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $months = $this->months;

        for ($i = 1; $i <= 31; $i++) {
            $days[] = $i;
        }

        for ($i = 0; $i <= 23; $i++) {
            $hours[] = $i;
        }

        return view('dob.create', compact('months','days','hours'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the form data
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $name = $request->name;
        $birthDate = Carbon::create($request->year, $request->month, $request->day+1, $request->hour, 0, 0);
        $id = dob::create(['name' => $name, 'birthdate' => $birthDate])->id;
        return redirect('dob/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dob = dob::findOrFail($id);
        $age = Carbon::now()->diff(new Carbon($dob->birthdate))->format('%y years, %m months, %d days, %h hours');
        return view('dob.show', compact('dob', 'age'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
